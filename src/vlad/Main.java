package vlad;

public class Main {

    public static void main(String[] args) {
        Family family = new Family(new Human("Elena", "Lietun"), new Human("Dmitiy", "Lietun"));

        family.addChild(new Human("Vlad", "Lietun"));
        family.addChild(new Human("adssd", "21122"));
        family.addChild(new Human("reweweq", "mbmbmbm"));
        System.out.println(family);

        family.deleteChild(1);
        System.out.println(family);

        family.deleteChild(new Human("reweweq", "mbmbmbm"));
        System.out.println(family);

        System.out.println(family.countFamily());

    }
}

